# CleanAirApp

General Ventilation Controller App

This is a general Homey App for controlling Home Ventilation with modbus TCP supported.

The plan is to support different aggregates, with HERU 62T and NILAN Comfort 350 as the primary goal. Others may follow.

This project is inspired by Bjørnar Almlis https://github.com/balmli/com.systemair