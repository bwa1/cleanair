'use strict';

const Homey = require('homey');

class CleanAirApp extends Homey.App {

  /**
   * onInit is called when the app is initialized.
   */
  async onInit() {
    this.log('Cleanair App has been initialized');
  }

}

module.exports = CleanAirApp;
